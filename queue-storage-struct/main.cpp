#include <iostream>
#include "queue.hpp"

using namespace std;

int main()
{
	queue_t<int> queue(10);

	for (int i = 1; i <= 11; i++)
		queue.push(i);

	while (queue.pop())
		cout << queue.get_top() << "; Size: " << queue.getSize() << endl;

	return 0;
}