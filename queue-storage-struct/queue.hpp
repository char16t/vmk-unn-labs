#ifndef _QUEUE_HPP_
#define _QUEUE_HPP_

#include <iostream>

using namespace std;

template <class DataType>
class queue_t
{
public:
	queue_t();
	queue_t(int);
	queue_t(const queue_t&);
	~queue_t();
	void push(const DataType&);
	DataType pop();
	DataType get_top();
	int getSize() const;
private:
	void resize(int);
	DataType *data;
	int top;
	int size;
};

template <class DataType>
queue_t<DataType>::queue_t() :data(0), top(0), size(0)
{}

template <class DataType>
queue_t<DataType>::queue_t(int _size) : size(_size), top(0)
{
	if (size)
		data = new DataType[size];
	else
		data = 0;
}

template <class DataType>
queue_t<DataType>::queue_t(const queue_t& stack)
{
	if (stack.size)
	{
		size = stack.size;
		data = new DataType[size];
		top = 0;
	}
	else
	{
		data = 0;
		top = size = 0;
	}
}

template <class DataType>
queue_t<DataType>::~queue_t()
{
	delete[] data;
}

template <class DataType>
void queue_t<DataType>::push(const DataType& item)
{
	if (top >= size)
		resize(size + 1);
	
	for (int i = top; i > 0; i--)
		data[i] = data[i-1];
	
	data[0] = item;
	top++;
}

template <class DataType>
DataType queue_t<DataType>::pop()
{
	if (top > size) {
		top--;
		return data[top-1];
	}
	else if (top>1)
	{
		resize(size-1);
		top--;
		return data[top-1];
	}
	else
		return (DataType)0;
}

template <class DataType>
DataType queue_t<DataType>::get_top()
{
	if (top > 0)
		return data[top - 1];
}

template <class DataType>
int queue_t<DataType>::getSize() const
{
	return size;
}

template <class DataType>
void queue_t<DataType>::resize(int newsize)
{
	DataType *newdata;
	if (newsize)
	{
		newdata = new DataType[newsize];
		memcpy(newdata, data,
			((size>newsize) ? newsize : size)*sizeof(DataType));
		size = newsize;
		delete[]data;
		data = newdata;
	}
}

#endif