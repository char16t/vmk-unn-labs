#include <iostream>
#include <conio.h>
#include "utmatrix.hpp"

using namespace std;

int main() {
    utmatrix_t a;
    utmatrix_t b(10), c(10), d(10), e(3), g(2), h(2), m(2);
    
    //cout << "size(a) = " << a.size() << endl;
    //cout << "size(b) = " << b.size() << endl;
    //cout << "a.set_element(0,0,3);" << endl; a(0,0) = 3;
    //cout << "a.get_element(0,0) = " << a(0,0) << endl;

    //cout << "b.set_element(0,1,5);" << endl; a(0,1) = 5;
    //cout << "b.get_element(0,1) = " << a(0,1) << endl;

    //cout << "c.set_element(0,1,5)" << endl; c(0,1) = 5;
    //cout << "d = b + c;" << endl; 
    //cout << "d.get_element(0,1) = " << d(0,1) << endl;
    
    b(9,9) = 3;
    c(9,9) = 4;
	d = b + c;
    cout << d << endl;
    
    d = b - c;
    cout << d << endl;

    g(0,0) = 1;
    g(0,1) = 2;
    g(1,1) = 3;

    h(0,0) = 4;
    h(0,1) = 5;
    h(1,1) = 6;
    
    m = g*h;
    cout << "m = " << m << endl;
    cout << "det m = " << m.det() << endl;
    
    
    cout << e << endl;
    cout << "Input matrix, please (3x3). Example: {1,1,1,2,2,4}: ";
    cin >> e;
    cout << e << endl;
    
    _getch();
    return 0;
}