#ifndef UTMATRIX_HPP
#define UTMATRIX_HPP

#include <iostream>
using namespace std;


const int DEFAULT_MATRIX_SIZE = 4;

class utmatrix_t {
public:
    utmatrix_t();
    utmatrix_t(int);
    utmatrix_t(const utmatrix_t&);
   ~utmatrix_t();
    int size();
    int det();
    int& operator()(int i, int j);
    int operator()(int i, int j) const;
    utmatrix_t& utmatrix_t::operator=(const utmatrix_t& o);
	utmatrix_t operator+(const utmatrix_t&);
    utmatrix_t operator-(const utmatrix_t&);
    utmatrix_t operator*(const utmatrix_t&);
    friend istream& operator>>(istream&, utmatrix_t&);
    friend ostream& operator<<(ostream&, utmatrix_t&);
private:
    int  n;
    int* data;
};

#endif