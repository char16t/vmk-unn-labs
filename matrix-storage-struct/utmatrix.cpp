#include <iostream>
#include "utmatrix.hpp"

using namespace std;

int _NULL = 0;

utmatrix_t::utmatrix_t() {
    n = DEFAULT_MATRIX_SIZE;
    data = new int[ n*(n+1)/2 ]; 
    
    for(int i=0; i<n*(n+1)/2; i++)
        data[i] = 0;
}

utmatrix_t::utmatrix_t(int size) {
    n    = size;
    data = new int[ n*(n+1)/2 ]; 

    for(int i=0; i<n*(n+1)/2; i++)
        data[i] = 0;    
}

utmatrix_t::utmatrix_t(const utmatrix_t& o) {
    n = o.n;
    data = new int[n*(n + 1) / 2];

    for (int i = 0; i < n*(n + 1) / 2; i++)
        data[i] = o.data[i];
}

utmatrix_t::~utmatrix_t() {
    n=0;
    delete [] data;
}

int utmatrix_t::size() {
    return n;
}

int utmatrix_t::det() {
    int r = 1, elem;
    for(int i=0; i<n; i++) {
        elem = operator()(i,i);
        if(elem == 0) return 0;
        r *= elem;
    }
    
    return r;
}

int& utmatrix_t::operator()(int i, int j)
{
    if(i<=j) {
        int index = ((n*(n+1)-(n-i)*(n-i+1))/2) + j - i;
        return data[index];
	} else {
        return _NULL;
    }
}

int utmatrix_t::operator()(int i, int j) const
{
    if(i<=j) {
        int index = ((n*(n+1)-(n-i)*(n-i+1))/2) + j - i;
        return data[index];
    } else {
        return 0;
    }
}

utmatrix_t& utmatrix_t::operator=(const utmatrix_t& o) {
    n = o.n;
    data = new int[n*(n + 1) / 2];

    for (int i = 0; i < n*(n + 1) / 2; i++)
        data[i] = o.data[i];

    return *this;
}

utmatrix_t utmatrix_t::operator+(const utmatrix_t& o) {
    utmatrix_t product(n);

    for (int i = 0; i < n*(n + 1) / 2; i++)
        product.data[i] = data[i] + o.data[i];

    return product;
}

utmatrix_t utmatrix_t::operator-(const utmatrix_t& o) {
    utmatrix_t product(n);

    for (int i = 0; i < n*(n + 1) / 2; i++)
        product.data[i] = data[i] - o.data[i];

    return product;
}

utmatrix_t utmatrix_t::operator*(const utmatrix_t& o) {
    utmatrix_t product(n);
    
    for (int row = 0; row < n; row++)
        for (int col = 0; col < n; col++)
            for (int inner = 0; inner < n; inner++)
                product(row, col) += operator()(row, inner) * o(inner, col);
                
    return product;
}

istream& operator>>(istream& istr, utmatrix_t &o) {
    char ch;
    int i=0,j=0,tmp;
    int n = o.size();
    
    do
        istr >> ch;
    while (ch != '{');
    
    for (int i = 0; i < n; i++)
        for (int j = i; j < n; j++) {
            istr >> tmp;
            o(i, j) = tmp;
            
            do istr >> ch;
            while ((ch != ',') && (ch != '}'));
        }
        
    return istr;
}
    
ostream& operator<<(ostream& ostr, utmatrix_t &o) {
    int i=0, j=0; 
    char ch= ' ';
    int n = o.size();
    
    for (int i = 0; i < n; i++) {
        ostr << "[";
        for (int j = 0; j < n; j++) {
            ostr << ch << ' ' << o(i, j);
            ch = ' ';
        }
        ostr << "]" << endl;
    }
    return ostr;
}