#ifndef STACK_HPP
#define STACK_HPP

#include <cstddef>

const int max_size = 4;

template<class T>
class stack_t{
public:
    stack_t();
   ~stack_t();
	bool push(T val);
    bool pop();
	T top();
private:
    struct Node {
        T x;
        Node *next;
    };
	Node *head;
	
	int size;
};

template <class T>
stack_t<T>::stack_t()
{
    head = NULL;
}

template <class T>
stack_t<T>::~stack_t()
{
    Node** q = &head;
    Node** p;
    
	while (*q) {
        p = &(*q)->next;
        delete *q;
        q = p;
    }
    
    size = 0; 
}

template <class T>
bool stack_t<T>::push(T val)
{
	if(size == max_size)
	{
		return false;
	}
    else
    {
        Node *n = new Node();
        n->x = val;
        n->next = head;
        head = n;
        
        return true;
    }
}

template <class T>
bool stack_t<T>::pop()
{
	if( head == NULL )
		return 1;
	
    Node *n = head;

    head = head->next;
    delete n;
	
	return 0;
}

template <class T>
T stack_t<T>::top()
{
    Node *n = head;
    T ret = n->x;
	
    return ret;
}

#endif