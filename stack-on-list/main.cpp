#include "stack.hpp"
#include <iostream>

using namespace std;

int main() {
    stack_t<float> s;

    s.push(5);
    s.push(10);
    s.push(20);

    cout << s.top() << endl; s.pop();
	cout << s.top() << endl; s.pop();
	cout << s.top() << endl; s.pop();
	
	cout << s.pop() << endl;
	
    return 0;
}