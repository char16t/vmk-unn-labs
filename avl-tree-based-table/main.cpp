#include <iostream>
#include "table.hpp"

int main() {
	table_t<int, char*> a;

	a.put(1, "It's works!");
	std::cout << a.get(1) << std::endl;
	a.del(1);

	return 0;
}