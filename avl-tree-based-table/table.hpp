#ifndef TABLE_HPP
#define TABLE_HPP

#define NULL 0

template <class key_t, class value_t>
struct node_t
{
	key_t key;
	value_t value;
	unsigned char height;
	node_t<key_t, value_t>* left;
	node_t<key_t, value_t>* right;
	node_t() { key = 0; value = 0; left = right = 0; height = 1; }
	node_t(key_t k, value_t v) { key = k; value = v; left = right = 0; height = 1; }
	~node_t() { if (left) delete left; if (right) delete right; }
};

template <class key_t, class value_t>
class table_t {
public:
	table_t();
	table_t(key_t, value_t);
   ~table_t();
    void    put(key_t, value_t);
	value_t get(key_t);
	void    del(key_t);
private:
	unsigned char height(node_t<key_t, value_t>* p);
	int bfactor(node_t<key_t, value_t>* p);
	void fixheight(node_t<key_t, value_t>* p);
	node_t<key_t, value_t>* rotateright(node_t<key_t, value_t>* p);
	node_t<key_t, value_t>* rotateleft(node_t<key_t, value_t>* q);
	node_t<key_t, value_t>* balance(node_t<key_t, value_t>* p);
	node_t<key_t, value_t>* insert(node_t<key_t, value_t>* p, key_t, value_t);
	node_t<key_t, value_t>* findmin(node_t<key_t, value_t>* p);
	node_t<key_t, value_t>* removemin(node_t<key_t, value_t>* p);
	node_t<key_t, value_t>* remove(node_t<key_t, value_t>* p, key_t);
	node_t<key_t, value_t>* find(node_t<key_t, value_t>* p, key_t);
	node_t<key_t, value_t>* root;
};

template <class key_t, class value_t>
table_t<key_t, value_t>::table_t() {
	root = new node_t<key_t, value_t>;
	root->key = 0;
	root->value = 0;
}

template <class key_t, class value_t>
table_t<key_t, value_t>::table_t(key_t key, value_t value) {
	root = new node_t<key_t, value_t>;
	root->key   = key;
	root->value = value;
}

template <class key_t, class value_t>
table_t<key_t, value_t>::~table_t() {
	delete root;
}

template <class key_t, class value_t>
void table_t<key_t, value_t>::put(key_t key, value_t value) {
	insert(root, key, value);
}

template <class key_t, class value_t>
value_t table_t<key_t, value_t>::get(key_t key) {
	node_t<key_t, value_t>* found = find(root, key);
	return found ? found->value : (value_t)NULL;
}

template <class key_t, class value_t>
void table_t<key_t, value_t>::del(key_t key) {
	remove(root, key);
}

template <class key_t, class value_t>
unsigned char table_t<key_t, value_t>::height(node_t<key_t, value_t>* p)
{
	return p ? p->height : 0;
}

template <class key_t, class value_t>
int table_t<key_t, value_t>::bfactor(node_t<key_t, value_t>* p)
{
	return height(p->right) - height(p->left);
}

template <class key_t, class value_t>
void table_t<key_t, value_t>::fixheight(node_t<key_t, value_t>* p)
{
	unsigned char hl = height(p->left);
	unsigned char hr = height(p->right);
	p->height = (hl>hr ? hl : hr) + 1;
}

template <class key_t, class value_t>
node_t<key_t, value_t>* table_t<key_t, value_t>::rotateright(node_t<key_t, value_t>* p)
{
	node_t<key_t, value_t>* q = p->left;
	p->left = q->right;
	q->right = p;
	fixheight(p);
	fixheight(q);
	return q;
}

template <class key_t, class value_t>
node_t<key_t, value_t>* table_t<key_t, value_t>::rotateleft(node_t<key_t, value_t>* q)
{
	node_t<key_t, value_t>* p = q->right;
	q->right = p->left;
	p->left = q;
	fixheight(q);
	fixheight(p);
	return p;
}

template <class key_t, class value_t>
node_t<key_t, value_t>* table_t<key_t, value_t>::balance(node_t<key_t, value_t>* p)
{
	fixheight(p);
	if (bfactor(p) == 2)
	{
		if (bfactor(p->right) < 0)
			p->right = rotateright(p->right);
		return rotateleft(p);
	}
	if (bfactor(p) == -2)
	{
		if (bfactor(p->left) > 0)
			p->left = rotateleft(p->left);
		return rotateright(p);
	}
	return p;
}

template <class key_t, class value_t>
node_t<key_t, value_t>* table_t<key_t, value_t>::insert(node_t<key_t, value_t>* p, key_t k, value_t v)
{
	if (!p) return new node_t<key_t, value_t>(k, v);
	if (k<p->key)
		p->left = insert(p->left, k, v);
	else
		p->right = insert(p->right, k, v);
	return balance(p);
}

template <class key_t, class value_t>
node_t<key_t, value_t>* table_t<key_t, value_t>::findmin(node_t<key_t, value_t>* p)
{
	return p->left ? findmin(p->left) : p;
}

template <class key_t, class value_t>
node_t<key_t, value_t>* table_t<key_t, value_t>::removemin(node_t<key_t, value_t>* p)
{
	if (p->left == 0)
		return p->right;
	p->left = removemin(p->left);
	return balance(p);
}

template <class key_t, class value_t>
node_t<key_t, value_t>* table_t<key_t, value_t>::remove(node_t<key_t, value_t>* p, key_t k)
{
	if (!p) return 0;
	if (k < p->key)
		p->left = remove(p->left, k);
	else if (k > p->key)
		p->right = remove(p->right, k);
	else //  k == p->key 
	{
		node_t<key_t, value_t>* q = p->left;
		node_t<key_t, value_t>* r = p->right;
		delete p;
		if (!r) return q;
		node_t<key_t, value_t>* min = findmin(r);
		min->right = removemin(r);
		min->left = q;
		return balance(min);
	}
	return balance(p);
}

template <class key_t, class value_t>
node_t<key_t, value_t>* table_t<key_t, value_t>::find(node_t<key_t, value_t>* p, key_t k)
{
	if (!p)
		return NULL;
	else if (k == p->key)
		return p;
	else if (k<p->key)
		p->left = find(p->left, k);
	else if (k > p->key)
		p->right = find(p->right, k);
}

#endif