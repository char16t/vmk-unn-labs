#include <iostream>
#include <string>
#include <conio.h>
#include "table.hpp"

using namespace std;

int main() {
    /* table1() */
    utable_t<string, int> table1;
    
    /* operator[] */
    table1.set("s", 2);
	try {
		cout << "table1[\"s\"] = " << table1.get("s") << endl;
	}
	catch (int i) {
		if (i == 1)
			cout << "table1[\"s\"] not found" << endl;
	}

	table1.set("t", 3);
	try {
		cout << "table1[\"t\"] = " << table1.get("t") << endl;
	}
	catch (int i) {
		if (i == 1)
			cout << "table1[\"t\"] not found" << endl;
	}

	table1.set("r", 1);
	try {
		cout << "table1[\"s\"] = " << table1.get("r") << endl;
	}
	catch (int i) {
		if (i == 1)
			cout << "table1[\"r\"] not found" << endl;
	}

	table1.del("s");
	try {
		cout << "table1[\"s\"] = " << table1.get("s") << endl;
	} catch (int i) {
		if (i == 1)
			cout << "table1[\"s\"] not found" << endl;
	}

    cout << "Press any key for exit..." << endl;
    getch();
    return 0;
}