#ifndef UTABLE_HPP
#define UTABLE_HPP

template <class key_t, class data_t>
class utable_t {
public:
    utable_t();
    utable_t(int size);
    utable_t(const utable_t&);
   ~utable_t();
    utable_t& operator=(const utable_t&);
    data_t    get(key_t);
    void      set(key_t, data_t);
    key_t     find(data_t);
    bool      del(key_t);
private:
    int search_record(key_t);
    
    struct Node {
        data_t data;
        key_t  key;
    };
    
    Node* storage;
    int   size;
    
    void resize(int);
};

template <class key_t, class data_t>
utable_t<key_t, data_t>::utable_t() {
    size    = 0;
    storage = 0;
}

template <class key_t, class data_t>
utable_t<key_t, data_t>::utable_t(int size) {
    this->size    = size;
    this->storage = new Node[size];
}

template <class key_t, class data_t>
utable_t<key_t, data_t>::utable_t(const utable_t& o) {
        for(int i=0; i<size; i++)
            o[ storage[i].key ] = storage[i].data;
        
        o.size = size;
}

template <class key_t, class data_t>
utable_t<key_t, data_t>::~utable_t() {
   delete [] storage;
   size=0;
}

template <class key_t, class data_t>
utable_t<key_t, data_t>& utable_t<key_t, data_t>::operator=(const utable_t& o) {
    if(this != &o) {
        for(int i=0; i<size; i++)
            o[ storage[i].key ] = storage[i].data;
            
        o.size = size;
    }
    
    return *this;
}

template <class key_t, class data_t>
data_t utable_t<key_t, data_t>::get(key_t key) {
    int pos = search_record(key);
	if (pos != -1)
		return storage[pos].data;
	else
		throw 1;
        //return (data_t)0;
}

template <class key_t, class data_t>
void utable_t<key_t, data_t>::set(key_t key, data_t data) {

    /* get insert position */
    int insert_position=-1;
    int s = 0;
    int p = 0;
    int q = size-1;
    
    while ( p <= q ) {
        s = p + (q-p)/2;
        if(storage[s].key == key) {
            insert_position = s;
            break;
        }
        else if (storage[s].key < key)
            p = s+1;
        else if (storage[s].key > key)
            q = s-1;
    }

    if( insert_position == -1 )
        insert_position = p;
    
    /* resize */
    resize(size+1);
    
    /* move */
    for(int i=size-1; i>insert_position; i--)
        storage[i] = storage[i-1];
    
    /* insertion */
    storage[insert_position].key = key;
    storage[insert_position].data = data;
}

template <class key_t, class data_t>
key_t utable_t<key_t, data_t>::find(data_t data) {
    for(int i=0; i<size; i++)
        if(storage[i].data == data)
            return storage[i].key; 
            
    return (key_t)0;
}

template <class key_t, class data_t>
bool utable_t<key_t, data_t>::del(key_t key) {
	/* get remove position */
	int remove_position = -1;
	int s = 0;
	int p = 0;
	int q = size - 1;

	while (p <= q) {
		s = p + (q - p) / 2;
		if (storage[s].key == key) {
			remove_position = s;
			break;
		}
		else if (storage[s].key < key)
			p = s + 1;
		else if (storage[s].key > key)
			q = s - 1;
	}

	if (remove_position == -1)
		return false;

	/* move */
	for (int i = remove_position; i<size-1; i++)
		storage[i] = storage[i + 1];

	/* resize */
	resize(size-1);
}

template <class key_t, class data_t>
void utable_t<key_t, data_t>::resize(int newsize) {
	int i;
    Node tmp;
    Node *newdata;
	
    newdata = new Node[newsize];
    if( size > newsize ) {
        for(i = 0; i < newsize; i++)
            newdata[i] = storage[i];
    } else {
        for(i = 0; i < size; i++)
            newdata[i] = storage[i];
        
        for(; i < newsize; i++)
            newdata[i] = tmp;
    }
        
	size = newsize;
	delete [] storage;
	storage = newdata;
}

template <class key_t, class data_t>
int utable_t<key_t, data_t>::search_record(key_t key) {
    int s = 0;
    int p = 0;
    int q = size-1;
    
    while ( p <= q ) {
        s = p + (q-p)/2;
        if(storage[s].key == key)
            return s;
        else if (storage[s].key < key)
            p = s+1;
        else if (storage[s].key > key)
            q = s-1;
    }
    
    return -1;
}

#endif