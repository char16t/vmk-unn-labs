#include "queue.hpp"
#include <iostream>

using namespace std;

int main() {
    queue_t<float> s;

    s.append(5);
    s.append(10);
    s.append(20);
	s.append(30);
	s.append(40);
	s.append(50);

    cout << s.top() << endl; s.pop();
	cout << s.top() << endl; s.pop();
	
    return 0;
}