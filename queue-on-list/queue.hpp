#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <cstddef>

const int max_size = 4;

template<class T>
class queue_t {
public:
    queue_t();
   ~queue_t();
	void append(T x);
    T pop();
	T top();
private:
	struct Node {
        T x;
        Node *next;
    };
	
	Node *head;
	int size;
};

template <class T>
queue_t<T>::queue_t()
{
    head = NULL;
}

template <class T>
queue_t<T>::~queue_t()
{
    Node** q = &head;
    Node** p;
    
	while (*q) {
        p = &(*q)->next;
        delete *q;
        q = p;
    }
    
    size = 0; 
}

template <typename T>
void queue_t<T>::append(T data)
{
    if(size == max_size)
		pop();
	
	Node** q = &head;
		
	while (*q)
		q = &(*q)->next;

	*q = new Node;
	(*q)->x = data;
	(*q)->next = NULL;
		
	size++;
}

template <class T>
T queue_t<T>::pop()
{
	if( !head )
		return 1;
	
    Node *n = head;
    T ret = n->x;

    head = head->next;
    delete n;
	
	size--;
	
	return 0;
}

template <class T>
T queue_t<T>::top()
{
    Node *n = head;
    T ret = n->x;
	
    return ret;
}

#endif