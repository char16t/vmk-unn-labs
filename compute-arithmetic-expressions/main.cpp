#include <iostream>
#include <stack>
#include <string>

using namespace std;

namespace calc {

bool delimiter (char c) {
	return c == ' ';
}

bool is_operator (char c) {
	if( c=='+' ) return true;
    if( c=='-' ) return true;
    if( c=='*' ) return true;
    if( c=='/' ) return true;
    
    return false;
}

bool is_operand(char c) {
    switch (c) {
        case '0': return true;
        case '1': return true;
        case '2': return true;
        case '3': return true;
        case '4': return true;
        case '5': return true;
        case '6': return true;
        case '7': return true;
        case '8': return true;
        case '9': return true;
        case '.': return true;
        default: return false;
    }
}
 
int priority (char op) {
	switch (op) {
		case '+': return 1; break;
        case '-': return 1; break;
        case '*': return 2; break;
        case '/': return 2; break;
        default : return -1; break;
    }
}
 
template <typename T>
void compute (stack<T> & st, char op) {
	T r = st.top();  st.pop();
	T l = st.top();  st.pop();
	switch (op) {
		case '+':  st.push (l + r);  break;
		case '-':  st.push (l - r);  break;
		case '*':  st.push (l * r);  break;
		case '/':  st.push (l / r);  break;
	}
}

template <typename T> 
T exec (string & s) {
	stack<T> st;
	stack<char> op;
	for (int i=0; i<s.length(); ++i) {
		if (!delimiter (s[i])) {
			if (s[i] == '(')
				op.push ('(');
			else if (s[i] == ')') {
				while (op.top() != '(') {
					compute<T>(st, op.top());
                    op.pop();
                }
				op.pop();
			}
			else if (is_operator (s[i])) {
				char curop = s[i];
				while (!op.empty() && priority(op.top()) >= priority(s[i])) {
					compute<T>(st, op.top());
                    op.pop();
                }
				op.push (curop);
			}
			else {
				string operand;
				while (i < s.length() && is_operand(s[i]))
					operand += s[i++];
				--i;
                st.push (atof (operand.c_str()));
			}
        }
    }
    
    while (!op.empty()) {
		compute<T> (st, op.top());
        op.pop();
    }
    
	return st.top();
}

}; // namespace calc

int main() {
    string input = "";
    cout << "Example: (2+2)*2" << endl;
    cout << "For exit type \"exit\" " << endl << endl;
    
    while(input != "exit")
    {
        cout << "> ";
        cin >> input;
        if(input != "exit")
            cout << "> " << calc::exec<float>(input) << endl;
    }
}