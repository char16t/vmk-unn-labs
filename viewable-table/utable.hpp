#ifndef UTABLE_HPP
#define UTABLE_HPP

template <class key_t, class data_t>
class utable_t {
public:
    utable_t();
    utable_t(int size);
    utable_t(const utable_t&);
   ~utable_t();
    utable_t& operator=(const utable_t&);
    data_t    operator[](key_t) const;
    data_t&   operator[](key_t);
    key_t     find(data_t);
    bool      del(key_t);
private:
    struct Node {
        data_t data;
        key_t  key;
    };
    
    Node* storage;
    int   size;
    
    void resize(int);
};

template <class key_t, class data_t>
utable_t<key_t, data_t>::utable_t() {
    size    = 0;
    storage = 0;
}

template <class key_t, class data_t>
utable_t<key_t, data_t>::utable_t(int size) {
    this->size    = size;
    this->storage = new Node[size];
}

template <class key_t, class data_t>
utable_t<key_t, data_t>::utable_t(const utable_t& o) {
        for(int i=0; i<size; i++)
            o[ storage[i].key ] = storage[i].data;
        
        o.size = size;
}

template <class key_t, class data_t>
utable_t<key_t, data_t>::~utable_t() {
   delete [] storage;
   size=0;
}

template <class key_t, class data_t>
utable_t<key_t, data_t>& utable_t<key_t, data_t>::operator=(const utable_t& o) {
    if(this != &o) {
        for(int i=0; i<size; i++)
            o[ storage[i].key ] = storage[i].data;
            
        o.size = size;
    }
    
    return *this;
}

template <class key_t, class data_t>
data_t utable_t<key_t, data_t>::operator[](key_t key) const {
    for(int i=0; i<size; i++)
        if(storage[i].key == key)
            return storage[i].data;
    
    resize(size+1);
    storage[size-1].key = key;
    return storage[size-1].data;
}

template <class key_t, class data_t>
data_t& utable_t<key_t, data_t>::operator[](key_t key) {
    for(int i=0; i<size; i++)
        if(storage[i].key == key)
            return storage[i].data;
    
    resize(size+1);
    storage[size-1].key = key;
    return storage[size-1].data;
}

template <class key_t, class data_t>
key_t utable_t<key_t, data_t>::find(data_t data) {
    for(int i=0; i<size; i++)
        if(storage[i].data == data)
            return storage[i].key; 
            
    return (key_t)0;
}

template <class key_t, class data_t>
bool utable_t<key_t, data_t>::del(key_t key) {
    
    for(int i=0; i < size; i++) {
        if( storage[i].key == key ) {
            storage[i] = storage[ size-1 ];
            resize(size-1);
            return true;
        }
    }
    
    return false;
}

template <class key_t, class data_t>
void utable_t<key_t, data_t>::resize(int newsize) {
	int i;
    Node tmp;
    Node *newdata;
	
    newdata = new Node[newsize];
    if( size > newsize ) {
        for(i = 0; i < newsize; i++)
            newdata[i] = storage[i];
    } else {
        for(i = 0; i < size; i++)
            newdata[i] = storage[i];
        
        for(; i < newsize; i++)
            newdata[i] = tmp;
    }
        
	size = newsize;
	delete [] storage;
	storage = newdata;
}

#endif