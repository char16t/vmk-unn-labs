#include <iostream>
#include <string>
#include <conio.h>
#include "utable.hpp"

using namespace std;

int main() {
    /* table1() */
    utable_t<string, int> table1;
    
    /* operator[] */
    table1["s"] = 1;
    cout << "table1[\"s\"] = " << table1["s"] << endl;
    
    /* find() */
	string str = table1.find(1);
	cout << "table1.find(1) = " << str << endl ;

    /* del() */
    if( table1.del("s") ) {
        cout << "Key 's' was deleted from table1" << endl;
        cout << "table1[\"s\"] = " << table1["s"] << endl;
    } else {
        cout << "Key 's' was not deleted from table1" << endl;
    }
    
    if( table1.del("not found") ) {
        cout << "Key 'not found' was deleted from table1" << endl;
        cout << "table1[\"not found\"] = " << table1["not found"] << endl;
    } else {
        cout << "Key 'not found' was not deleted from table1" << endl;
    }
    
    /* table2(size) */
    utable_t<string, int> table2(10);
    
    /* operator[] */
    table2["ss"] = 2;
    cout << "table2[\"ss\"] = " << table2["ss"] << endl;
    cout << "table2[\"not found\"] = " << table2["not found"] << endl;
    
    cout << "Press any key for exit..." << endl;
    getch();
    return 0;
}