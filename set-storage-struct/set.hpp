#ifndef SET_HPP
#define SET_HPP

#include <iostream>
#include "bitfield.hpp"

using namespace std;

class set_t
{
public:
	set_t(int);
	set_t(const set_t&);
	set_t(const bitfield_t&);
	operator bitfield_t();
	int get_max_power(void) const;
	void insert(const int);
	void remove(const int);
	int is_exist(const int n);
	set_t& operator=(const set_t&);
	bool   operator==(const set_t&);
	set_t  operator+(const set_t&);
	set_t  operator*(const set_t&);
	set_t  operator~(void);
	friend istream &operator>>(istream &istr, set_t &s);
	friend ostream &operator<<(ostream &ostr, set_t &s);
private:
	int max_size;
	bitfield_t storage;
};

#endif