#include <iostream>
#include <string> 
#include "bitfield.hpp"
#include "set.hpp"

using namespace std;

int main()
{
    int input = 1;
    set_t a(30);
    set_t b(30);
    int e;
    
    cout << "10. Print set A" << endl;
    cout << "11. Insert into set A" << endl;
    cout << "12. Remove from set A" << endl;
    cout << "13. set A = set B" << endl;
    cout << "14. Find element in set A" << endl;
    cout << "15. Print ~set A" << endl;
    cout << "20. Print set B" << endl;
    cout << "21. Insert into set B" << endl;
    cout << "22. Remove from set B" << endl;
    cout << "23. set B = set A" << endl;
    cout << "24. Find element in set B" << endl;
    cout << "25. Print ~set B" << endl;
    cout << "30. Check set A == set B" << endl;
    cout << "31. Print set A * set B" << endl;
    cout << "32. Print set A + set B" << endl;
    cout << "0 . Exit" << endl;
    cout << endl;
    while(input != 0) {
        cout << "> ";
        cin >> input;
        
        switch(input) {
            case 10:
                cout << "set A = " << a << endl;
                break;
            case 11:
                cout << "Element: "; cin >> e;
                a.insert(e);
                break;
            case 12:
                cout << "Element: "; cin >> e;
                a.remove(e);
                break;
            case 13:
                a = b;
                break;
            case 14:
                cout << "Element: "; cin >> e;
                if (a.is_exist(e)) cout << "True" << endl; else cout << "False" << endl;
                break;
            case 15:
                cout << "~set A = " << ~a << endl;
                break;
            case 20:
                cout << "set B = " << b << endl;
                break;
            case 21:
                cout << "Element: "; cin >> e;
                b.insert(e);
                break;
            case 22:
                cout << "Element: "; cin >> e;
                a.remove(e);
                break;
            case 23:
                b = a;
                break;
            case 24:
                cout << "Element: "; cin >> e;
                if (b.is_exist(e)) cout << "True" << endl; else cout << "False" << endl;
                break;
            case 25:
                cout << "~set B = " << ~b << endl;
                break;
            case 30:
                if(a == b) cout << "set A == set B" << endl; else cout << "set A != set B" << endl;
                break;
            case 31:
                cout << "set A * set B = " << a * b << endl;
                break;
            case 32:
                cout << "set A + set B " << a + b << endl;
                break;
        }
    }
	return 0;
}