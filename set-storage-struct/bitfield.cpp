#include <iostream>
#include <algorithm>
#include "bitfield.hpp"

using namespace std;

bitfield_t::bitfield_t(int len):Max(len)
{
	Size = (len + 15) >> 4;
	arr = new int[Size];

	if (arr != NULL)
        for (int i(0); i < Size; i++) arr[i] = 0;
}

bitfield_t::bitfield_t(const bitfield_t &bf)
{
	Max = bf.Max;
	Size = bf.Size;
	arr = new int[Size];
	if (arr != NULL)
        for (int i(0); i < Size; i++) 
            arr[i] = bf.arr[i];
}

bitfield_t::~bitfield_t() 
{
	delete[]arr;
	arr = NULL;
}

int bitfield_t::get_mem_index(const int n) const
{
	return n >> 4;
}

int bitfield_t::get_mem_mask(const int n) const
{
	return 1 << (n & 15);
}

int bitfield_t::get_length(void) const
{
	return Max;
}

void bitfield_t::set_bit(const int n)
{
	if ((n >= 0) && (n < Max))
		arr[get_mem_index(n)] |= get_mem_mask(n);
}

int bitfield_t::get_bit(const int n)
{
	if ((n >= 0) && (n < Max))
		return arr[get_mem_index(n)] & get_mem_mask(n);
	return 0;
}

void bitfield_t::clr_bit(const int n)
{
	if ((n >= 0) && (n < Max))
		arr[get_mem_index(n)] &= ~get_mem_mask(n);
}

bitfield_t& bitfield_t::operator=(const bitfield_t &bf)
{
	Max = bf.Max;
	if (Size != bf.Size)
	{
		Size = bf.Size;
		if (arr != NULL)
			delete arr;
		arr = new int[Size];
	}
	if (arr != NULL)
	for (int i(0); i<Size; i++)
		arr[i] = bf.arr[i];
	return *this;
}

bool bitfield_t::operator==(const bitfield_t &bf)
{
	bool res = true;
	if (Max != bf.Max) res = false;
	else
	{
		for (int i(0); i<Size; i++)
		{
			if (arr[i] != bf.arr[i])
			{
				res = false;
				break;
			}
		}
	}

	return res;
}

bitfield_t bitfield_t::operator|(const bitfield_t &bf)
{
	int len = Max;

	if (bf.Max>len) len = bf.Max;

	bitfield_t temp(len);
	for (int i=0; i<min(Size,bf.Size); i++)
    {
        temp.arr[i] = arr[i];
        temp.arr[i] |= bf.arr[i];
    }
    
	return temp;
}

bitfield_t bitfield_t::operator&(const bitfield_t &bf)
{
	int len = Max;

	if (bf.Max>len) len = bf.Max;

	bitfield_t temp(len);
	for (int i(0); i<min(Size,bf.Size); i++)
    {
        temp.arr[i] = arr[i];
        temp.arr[i] &= bf.arr[i];
    }
        
	return temp;
}

bitfield_t bitfield_t::operator~(void)
{
	int len = Max;
	bitfield_t temp(len);
	for (int i(0); i<Size; i++) temp.arr[i] = ~arr[i];
	return temp;
}

istream &operator>>(istream &istr, bitfield_t &bf)
{
	char ch;
	int i(0);

    while (true)
    {
        istr >> ch;
        if (ch == '0') bf.clr_bit(i++); 
			
        else if (ch == '1') bf.set_bit(i++); else break;
    }
	
	return istr;
}

ostream & operator<<(ostream &ostr, bitfield_t &bf)
{
	int len = bf.get_length();
	for (int i=len; i>=0; i--)
	
	if (bf.get_bit(i)) ostr << '1'; else ostr << '0';
	
	return ostr;
}