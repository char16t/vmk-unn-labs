#include <iostream>
#include "set.hpp"
#include "bitfield.hpp"

using namespace std;

set_t::set_t(int mp):max_size(mp),storage(mp)
{

}

set_t::set_t(const set_t &s):max_size(s.max_size),storage(s.storage)
{

}

set_t::set_t(const bitfield_t &bf):max_size(bf.get_length()),storage(bf)
{

}

set_t::operator bitfield_t()
{
	bitfield_t tmp(this->storage);
	return tmp;
}

int set_t::get_max_power(void) const
{
	return max_size;
}

int set_t::is_exist(const int n)
{
	return storage.get_bit(n);
}

void set_t::insert(const int n)
{
	storage.set_bit(n);
}

void set_t::remove(const int n)
{
	storage.clr_bit(n);
}

set_t& set_t::operator=(const set_t &s)
{
	storage = s.storage;
	max_size = s.get_max_power();
	return *this;
}

bool set_t::operator==(const set_t&s)
{
	return storage == s.storage;
}

set_t set_t::operator+(const set_t &s)
{
	set_t tmp(storage | s.storage);
	return tmp;
}

set_t set_t::operator*(const set_t &s)
{
	set_t tmp(storage & s.storage);
	return tmp;
}

set_t set_t::operator~(void)
{
	set_t tmp(~storage);
	return tmp;
}

istream& operator>>(istream &istr, set_t &s)
{
	int tmp; char ch;
	do
        istr >> ch;
	while (ch != '{');

	do {
        istr >> tmp;
        s.insert(tmp);
        do
            istr >> ch;
		while ((ch != ',') && (ch != '}'));
    }
	while (ch != '}');
	
    return istr;
}

ostream& operator<<(ostream &ostr, set_t &s)
{
	char ch= ' ';
	ostr << "{";
	int n = s.get_max_power();
	for (int i(0); i < n; i++)
	{
		if (s.is_exist(i))
		{
			ostr << ch << ' ' << i;
			ch = ',';
		}
	}
	ostr << "}";
	return ostr;
}
