#ifndef BITFIELD_HPP
#define BITFIELD_HPP

#include <iostream>

using namespace std;

class bitfield_t
{
public:
    bitfield_t(int);
	bitfield_t(const bitfield_t&);
   ~bitfield_t();
	void set_bit(const int);
	int get_bit(const int);
	void clr_bit(const int);
    int get_length(void) const;
    int get_mem_index(const int n) const;
	int get_mem_mask(const int n) const;
	bitfield_t& operator=(const bitfield_t&);
	bool        operator==(const bitfield_t&);
	bitfield_t  operator|(const bitfield_t&); 
	bitfield_t  operator&(const bitfield_t&);
	bitfield_t  operator~(void);
	friend istream& operator>>(istream &istr, bitfield_t &bf);
	friend ostream& operator<<(ostream &ostr, bitfield_t &bf);
private:
	int Max;
	int *arr;
	int Size;
};

#endif