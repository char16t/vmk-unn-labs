#include <iostream>
#include "stack.hpp"

using namespace std;

int main()
{
	stack_t<int> stack(10);

	for (int i = 1; i <= 11; i++)
		stack.push(i);

	while (stack.pop())
		cout << stack.get_top() << "; Size: " << stack.getSize() << endl;

	return 0; 
} 