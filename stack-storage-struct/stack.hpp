#ifndef _STACK_HPP_
#define _STACK_HPP_

#include <iostream>

using namespace std;

template <class DataType>
class stack_t
{
public:
	stack_t();
	stack_t(int);
	stack_t(const stack_t&);
	~stack_t();
	void push(const DataType&);
	DataType pop();
	DataType get_top();
	int getSize() const;
private:
	void resize(int);
	DataType *data;
	int top;
	int size;
};

template <class DataType>
stack_t<DataType>::stack_t() :data(0), top(0), size(0)
{}

template <class DataType>
stack_t<DataType>::stack_t(int _size) : size(_size), top(0)
{
	if (size)
		data = new DataType[size];
	else
		data = 0;
}

template <class DataType>
stack_t<DataType>::stack_t(const stack_t& stack)
{
	if (stack.size)
	{
		size = stack.size;
		data = new DataType[size];
		top = 0;
	}
	else
	{
		data = 0;
		top = size = 0;
	}
}

template <class DataType>
stack_t<DataType>::~stack_t()
{
	delete[] data;
}

template <class DataType>
void stack_t<DataType>::push(const DataType& item)
{
	if (top >= size)
		resize(size + 1);
	data[top] = item;
	top++;
}

template <class DataType>
DataType stack_t<DataType>::pop()
{
	if (top > size) {
		top--;
		return data[top-1];
	}
	else if (top > 1)
	{
		resize(size-1);
		top--;
		return data[top-1];
	}
	else
		return (DataType)0;
}


template <class DataType>
DataType stack_t<DataType>::get_top()
{
	if (top > 0)
		return data[top - 1];
}

template <class DataType>
int stack_t<DataType>::getSize() const
{
	return size;
}

template <class DataType>
void stack_t<DataType>::resize(int newsize)
{
	DataType *newdata;
	if (newsize)
	{
		newdata = new DataType[newsize];
		memcpy(newdata, data,
			((size>newsize) ? newsize : size)*sizeof(DataType));
		size = newsize;
		delete[]data;
		data = newdata;
	}
}

#endif