#include <iostream>
#include "hashtable.hpp"

int main() {
	hashtable_t<int, char*> a;
	a.put(3, "Yeah!!\n");
	std::cout << a.get(3);
    a.put(3, "Yeah!\n");
    std::cout << a.get(3);
	a.remove(3);

	return 0;
}