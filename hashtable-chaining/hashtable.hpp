#ifndef HASHTABLE_HPP 
#define HASHTABLE_HPP

#define NULL 0
const int TABLE_SIZE = 128;

template <class key_t, class value_t>
class entry_t {
public:
	entry_t(key_t key, value_t value);
	inline key_t get_key();
	inline value_t get_value();
	inline void set_value(value_t value);
	inline entry_t<key_t, value_t> *get_next();
	inline void set_next(entry_t<key_t, value_t> *next);
private:
	key_t key;
	value_t value;
	entry_t<key_t, value_t> *next;
};

template <class key_t, class value_t>
entry_t<key_t, value_t>::entry_t(key_t key, value_t value) {
	this->key = key;
	this->value = value;
	this->next = NULL;
}

template <class key_t, class value_t>
key_t entry_t<key_t, value_t>::get_key() {
	return key;
}

template <class key_t, class value_t>
value_t entry_t<key_t, value_t>::get_value() {
	return value;
}

template <class key_t, class value_t>
void entry_t<key_t, value_t>::set_value(value_t value) {
	this->value = value;
}

template <class key_t, class value_t>
entry_t<key_t, value_t> *entry_t<key_t, value_t>::get_next() {
	return next;
}

template <class key_t, class value_t>
void entry_t<key_t, value_t>::set_next(entry_t<key_t, value_t> *next) {
	this->next = next;
}

template <class key_t, class data_t>
class hashtable_t {
private:
	entry_t<key_t, data_t> **table;
public:
	hashtable_t();
	data_t get(key_t key);
	void put(key_t key, data_t value);
	void remove(key_t key);
	~hashtable_t();
};

template <class key_t, class data_t>
hashtable_t<key_t, data_t>::hashtable_t() {
	table = new entry_t<key_t, data_t>*[TABLE_SIZE];
	for (int i = 0; i < TABLE_SIZE; i++)
		table[i] = NULL;
}

template <class key_t, class data_t>
data_t hashtable_t<key_t, data_t>::get(key_t key) {
	int hash = (key % TABLE_SIZE); // ���� ���-�� ���������� ���������� ���-������� � �������� ���������
	if (table[hash] == NULL)
		return (data_t)-1; // ���� ��������� ���������� : ���� ������� ��� ������, � �� -1
	else {
		entry_t<key_t, data_t> *entry = table[hash];
		while (entry != NULL && entry->get_key() != key)
			entry = entry->get_next();
		
        if (entry == NULL)
			return (data_t)-1; // ���� �������� ��� � ��������� ���������� : ���� ������� ��� ������, � �� -1
		else
			return entry->get_value();
	}
}

template <class key_t, class data_t>
void hashtable_t<key_t, data_t>::put(key_t key, data_t value) {
	int hash = (key % TABLE_SIZE); // ���� ���-�� ���������� ���������� ���-�������
	if (table[hash] == NULL)
		table[hash] = new entry_t<key_t, data_t>(key, value);
	else {
		entry_t<key_t, data_t> *entry = table[hash];
        
		while (entry->get_next() != NULL) {
            if( entry->get_key() == key )
                break;
            else
                entry = entry->get_next();
        }
        
        if (entry->get_key() == key)
			entry->set_value(value);
		else
			entry->set_next(new entry_t<key_t, data_t>(key, value));
	}
}

template <class key_t, class data_t>
void hashtable_t<key_t, data_t>::remove(key_t key) {
	int hash = (key % TABLE_SIZE); // ���� ���-�� ���������� ���������� ���-�������

	if (table[hash] != NULL) {
		entry_t<key_t, data_t> *prev_entry = NULL;
		entry_t<key_t, data_t> *entry = table[hash];

		while (entry->get_next() != NULL && entry->get_key() != key) {
			prev_entry = entry;
			entry = entry->get_next();
		}

		if (entry->get_key() == key) {
			if (prev_entry == NULL) {
				entry_t<key_t, data_t> *next_entry = entry->get_next();
				delete entry;
				table[hash] = next_entry;
			} else {
				entry_t<key_t, data_t> *next = entry->get_next();
				delete entry;
				prev_entry->set_next(next);
			}
		}
	}
}

template <class key_t, class data_t>
hashtable_t<key_t, data_t>::~hashtable_t() {
	for (int i = 0; i < TABLE_SIZE; i++)
		if (table[i] != NULL) {
			entry_t<key_t, data_t> *prev_entry = NULL;
			entry_t<key_t, data_t> *entry = table[i];
			while (entry != NULL) {
				prev_entry = entry;
				entry = entry->get_next();
				delete prev_entry;
			}
		}
	delete[] table;
}

#endif