#ifndef HASHTABLE_HPP
#define HASHTABLE_HPP

#define NULL 0
const int TABLE_SIZE = 128;

template <class key_t, class value_t>
class entry_t {
public:
	entry_t(key_t key, value_t value);
	inline key_t get_key();
	inline value_t get_value();
	inline void set_value(value_t value);
private:
	key_t key;
	value_t value;
};

template <class key_t, class value_t>
entry_t<key_t, value_t>::entry_t(key_t key, value_t value) {
	this->key = key;
	this->value = value;
}

template <class key_t, class value_t>
key_t entry_t<key_t, value_t>::get_key() {
	return key;
}

template <class key_t, class value_t>
value_t entry_t<key_t, value_t>::get_value() {
	return value;
}

template <class key_t, class value_t>
void entry_t<key_t, value_t>::set_value(value_t value) {
	this->value = value;
}

template <class key_t, class value_t>
class deleted_entry_t : public entry_t<key_t, value_t> {
private:
	static deleted_entry_t<key_t, value_t> *entry;
	deleted_entry_t() : entry_t((key_t)-1, (value_t)-1) {}
public:
	static deleted_entry_t *get_unique_deleted_entry() {
		if (entry == NULL)
			entry = new deleted_entry_t();
		return entry;
	}
};

template <class key_t, class value_t>
deleted_entry_t<key_t, value_t> *deleted_entry_t<key_t, value_t>::entry = NULL;

template <class key_t, class data_t>
class hashtable_t {
private:
	entry_t<key_t, data_t> **table;

public:
	hashtable_t();
	data_t get(key_t key);
	void put(key_t key, data_t value);
	void remove(key_t key);
	~hashtable_t();
};

template <class key_t, class data_t>
hashtable_t<key_t, data_t>::hashtable_t() {
	table = new entry_t<key_t, data_t>*[TABLE_SIZE];
	for (int i = 0; i < TABLE_SIZE; i++)
		table[i] = NULL;
}

template <class key_t, class data_t>
data_t hashtable_t<key_t, data_t>::get(key_t key) {
	int hash = (key % TABLE_SIZE);
	int initialHash = -1;
	while (hash != initialHash && (table[hash] == deleted_entry_t<key_t, data_t>::get_unique_deleted_entry() || table[hash] != NULL && table[hash]->get_key() != key)) {
		if (initialHash == -1)
			initialHash = hash;
		hash = (hash + 1) % TABLE_SIZE;
	}

	if (table[hash] == NULL || hash == initialHash)
		return (data_t)-1;
	else
		return table[hash]->get_value();
}

template <class key_t, class data_t>
void hashtable_t<key_t, data_t>::put(key_t key, data_t value) {
	int hash = (key % TABLE_SIZE);
	int initialHash = -1;
	int indexOfdeleted_entry_t = -1;

	while (hash != initialHash && (table[hash] == deleted_entry_t<key_t, data_t>::get_unique_deleted_entry() || table[hash] != NULL && table[hash]->get_key() != key)) {
		if (initialHash == -1)
			initialHash = hash;

		if (table[hash] == deleted_entry_t<key_t, data_t>::get_unique_deleted_entry())
			indexOfdeleted_entry_t = hash;

		hash = (hash + 1) % TABLE_SIZE;
	}

	if ((table[hash] == NULL || hash == initialHash) && indexOfdeleted_entry_t != -1)
		table[indexOfdeleted_entry_t] = new entry_t<key_t, data_t>(key, value);
	else if (initialHash != hash)
		if (table[hash] != deleted_entry_t<key_t, data_t>::get_unique_deleted_entry() && table[hash] != NULL && table[hash]->get_key() == key)
			table[hash]->set_value(value);
		else
			table[hash] = new entry_t<key_t, data_t>(key, value);
}

template <class key_t, class data_t>
void hashtable_t<key_t, data_t>::remove(key_t key) {
	int hash = (key % TABLE_SIZE);
	int initialHash = -1;
	while (hash != initialHash && (table[hash] == deleted_entry_t<key_t, data_t>::get_unique_deleted_entry() || table[hash] != NULL && table[hash]->get_key() != key)) {
		if (initialHash == -1)
			initialHash = hash;

		hash = (hash + 1) % TABLE_SIZE;
	}

	if (hash != initialHash && table[hash] != NULL) {
		delete table[hash];
		table[hash] = deleted_entry_t<key_t, data_t>::get_unique_deleted_entry();
	}
}

template <class key_t, class data_t>
hashtable_t<key_t, data_t>::~hashtable_t() {
	for (int i = 0; i < TABLE_SIZE; i++)
		if ( table[i] != NULL && table[i] != deleted_entry_t<key_t, data_t>::get_unique_deleted_entry() )
			delete table[i];
	delete[] table;
}

#endif